import * as XLSX from 'xlsx';
import { Diputadx, diputadxsUrl } from './diputadxs';


export function createDiputadxList(sheet: XLSX.WorkSheet, props: Array<string>): Array<Diputadx> {

  const listaDiputadxs: Array<Diputadx> = new Array<Diputadx>();
  let currentDip: Diputadx;
  props.forEach(p => {
    switch(p.charAt(0)) {
      case 'A': //Nombre y Apellido
        currentDip = new Diputadx();
        currentDip.name = (sheet[p])["w"].split(',')[1].trim();
        currentDip.lastname = (sheet[p])["w"].split(',')[0].trim();
        currentDip.img = getUrlImg((sheet[p])["w"]);
        break;
      case 'B': //Distrito
        currentDip.distrito = (sheet[p])["w"];
        break;
      case 'C': //Genero
        currentDip.genero = (sheet[p])["w"];
        break;
      
      case 'F': //Bloque
        currentDip.bloque = mapBloqueName((sheet[p])["w"]);
        break;

      case 'G': //Despacho
        currentDip.despacho = (sheet[p])["w"];
        break;

      case 'H': //Interno
        currentDip.interno = (sheet[p])["w"];
        break;

      case 'I': //Twitter
        currentDip.twitter = (sheet[p])["w"].split('@')[0];
        break;

      case 'J': //Posicion
        currentDip.posicion = (sheet[p])["w"];
        listaDiputadxs.push(currentDip);
        break;
    }
  });
  console.log(listaDiputadxs);
  return listaDiputadxs;
}


function mapBloqueName(name: string): string {
  let mappedName: string;
  switch(name) {
    case "FRENTE CIVICO POR SANTIAGO":
      mappedName = "Frente Cívico por Santiago";
      break;

    case "PRO":
      mappedName = "PRO";  
      break;

    case "TODOS JUNTOS POR SAN JUAN":
      mappedName = "Todos Juntos por San Juan";
      break;

    case "FRENTE PARA LA VICTORIA - PJ":
      mappedName = "Frente para la Victoria - PJ";
      break;

    case "COMPROMISO FEDERAL":
      mappedName = "Compromiso Federal";
      break;

    case "UCR":
      mappedName = "UCR";
      break;

    case "FEDERAL UNIDOS POR UNA NUEVA ARGENTINA":
      mappedName = "Federal Unidos por una Nuevo Argentina";
      break;

    case "PARTIDO POR LA JUSTICIA SOCIAL":
      mappedName = "Partido por la Justicia Social";
      break;

    case "JUSTICIALISTA":
      mappedName = "Justicialista";
      break;

    case "FRENTE CIVICO Y SOCIAL DE CATAMARCA":
      mappedName = "Frente Cívico y Social de Catamarca";
      break;
    
    case "CORDOBA FEDERAL":
      mappedName = "Córdoba Federal";
      break;

    case "COALICION CIVICA":
      mappedName = "Coalición Cívica";
      break;
    
    case "EVOLUCION RADICAL":
      mappedName = "Evolución Radical";
      break;

    case "PARTIDO BLOQUISTA DE SAN JUAN":
      mappedName = "Partido Bloquista de San Juan";
      break;
    
    case "PARTIDO SOCIALISTA":
      mappedName = "Partido Socialista";
      break;

    case "PERONISMO PARA LA VICTORIA":
      mappedName = "Peronismo para la Victoria";
      break;
    
    case "PTS - FRENTE DE IZQUIERDA":
      mappedName = "PTS - Frente de Izquierda";
      break;

    case "FRENTE DE IZQUIERDA Y DE LOS TRABAJADORES":
      mappedName = "Frente de Izquierda y de los Trabajadores";
      break;
    
    case "FRENTE DE LA CONCORDIA MISIONERO":
      mappedName = "Frente de la Concordia Misionero";
      break;

    case "LIBRES DEL SUR":
      mappedName = "Libres del Sur";
      break;
    
    case "SOMOS MENDOZA":
      mappedName = "Somos Mendoza";
      break;

    case "ELIJO CATAMARCA":
      mappedName = "Elijo Catamarca";
      break;
    
    case "JUSTICIALISTA POR TUCUMAN":
      mappedName = "Justicialistas por Tucuman";
      break;

    case "TRABAJO Y DIGNIDAD":
      mappedName = "Trabajo y Dignidad";
      break;

    case "CORDOBA TRABAJO Y PRODUCCION":
      mappedName = "Córdoba Trabajo y Producción";
      break;

    case "SALTA SOMOS TODOS":
      mappedName = "Salta Somos Todos";
      break;

    case "PRIMERO TUCUMAN":
      mappedName = "Primero Tucuman";
      break;

    case "SOMOS SAN JUAN":
      mappedName = "Somos San Juan";
      break;
    
    case "CONCERTACION FORJA":
      mappedName = "Concentración FORJA";
      break;

    case "PARTIDO INTRANSIGENTE":
      mappedName = "Partido Intransigente";
      break;

    case "PRIMERO ARGENTINA":
      mappedName = "Primero Argentina";
      break;

    case "NUEVO ESPACIO SANTAFESINO":
      mappedName = "Nuevo Espacio Santafesino";
      break;

    case "MOV POP NEUQUINO":
      mappedName = "Movimiento Popular Neuquino";
      break;

    case "CHUBUT SOMOS TODOS":
      mappedName = "Chubut Somos Todos";
      break;

    default:
      mappedName = name;
      break;
  }
  return mappedName;
}

function getUrlImg(name: string): string {
    let url;
    diputadxsUrl.forEach(d => {
      if(d.Nombre === name) {
        url = d.Url;
      }
    });
    return url;
  }