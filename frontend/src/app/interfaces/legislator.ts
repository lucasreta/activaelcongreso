import { Inject } from '@angular/core';
import { environment } from '../../environments/environment';
import "rxjs/add/operator/map";
import * as _ from "lodash";
import { TweetsService } from '../services/tweets.service';


export enum LegislatorTypeEnum {Diputadx, Senadorx}

export class Legislator {
  constructor(
      public _tweetsService: TweetsService,
      public name: string,
      public lastname: string,
      public bloque: string,
      public posicion: string,
      public twitter: string,
      public despacho: string,
      public interno: string,
      public genero: string,
      public orden: number,
      public distrito: string,
      public key: string,
      public type: LegislatorTypeEnum,
      public facebook?: string,
      public disabled?: boolean,
      public img?: string
    ) {
  }

   public get dbpath(): string {
     if (this.type == LegislatorTypeEnum.Diputadx){
       return 'diputadxs';
     } else {
       return 'senadorxs';
     }
   }

   public get phoneNumber(): string {
     if (this.type == LegislatorTypeEnum.Diputadx) {
       return "+541141277100";
     } else {
       return "+541128223000";
     }
   }

   public get phoneString(): string {
     if (this.type == LegislatorTypeEnum.Diputadx) {
       return "(+54 11) 4127 - 7100";
     } else {
       return "(+54 11) 2822 - 3000";
     }
   }

  public get firstName(): string {
    return this.name.split(" ")[0];
  }

  public get getPosicion(): string {
    let posic: string;
    if(this.posicion == "No confirmado") {
      posic = "sinDefinir";
    } else if (this.posicion == "En Contra") {
      posic = "enContra";
    } else if (this.posicion == "A Favor") {
      posic = "aFavor";
    } else if (this.posicion == "Se Abstiene") {
      posic = "seAbstiene";
    }
    return posic;
  }

  public abortionTweet(): string {
    let saludo: string[] = [
      `Hola @${this.twitter}, `,
      `Che, @${this.twitter}, `,
      `Estimadx, @${this.twitter}, `
    ]
    
    let tweet = '';
    if(this.posicion == "No confirmado") {
      tweet = _.shuffle(this._tweetsService.tweetsNoconfirmado)[0];
    } else if (this.posicion == "En Contra") {
      tweet = _.shuffle(this._tweetsService.tweetsEnContra)[0];
    } else if (this.posicion == "A Favor") {
      tweet = _.shuffle(this._tweetsService.tweetsAFavor)[0];
    } else if (this.posicion == "Se Abstiene") {
      tweet = _.shuffle(this._tweetsService.tweetsSeAbstiene)[0];
    }

    const re = /_twitter_/gi;
    tweet = tweet.replace(re, `@${this.twitter}`);
    return encodeURIComponent(tweet);
  }

  public abortionFacebook(): string {
    if (this.posicion == "No confirmado") {
      return "Hola! Estuve buscando declaraciones tuyas pero no encontré nada que aclare cómo vas a votar sobre la ley de Interrupción Voluntaria del Embarazo. Queremos que el #AbortoLegal sea ley. Apoyanos con tu voto a favor. #ActivaElCongreso";
    } else if (this.posicion == "En Contra") {
      return "Hola! Esperamos que escuches a las miles de mujeres, jóvenes y a una sociedad que pide por el avance de sus derechos. Esperamos que cambies tu voto y apoyes el #AbortoLegal #ActivaElCongreso";
    } else if (this.posicion == "A Favor") {
      return "Holaaa! Vi que vas a votar a favor del #AbortoLegal. Miles de mujeres y personas gestantes te lo agradecerán. Yo también! #QueSeaLey #ActivaElCongreso";
    } else if (this.posicion == "Se Abstiene") {
      return 'Hola! No es momento de quedarse al margen de la historia. Necesitamos #QueSeaLey el aborto legal seguro y gratuito. #ActivaElCongreso';
    }

  }

}
