import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import * as _ from "lodash";

@Injectable()
export class TweetsService {

  tweetsAFavor: string[];
  tweetsEnContra: string[];
  tweetsNoconfirmado: string[];
  tweetsSeAbstiene: string[];
  

  constructor(public af: AngularFireDatabase) {
    this.af.list('/tweets/aborto/afavor').valueChanges().subscribe( val => {
      this.tweetsAFavor = [];
      val.forEach(e => this.tweetsAFavor.push(<string>e));
    });
    this.af.list('/tweets/aborto/encontra').valueChanges().subscribe( val => {
      this.tweetsEnContra = [];
      val.forEach(e => this.tweetsEnContra.push(<string>e));
    });
    this.af.list('/tweets/aborto/noconfirmado').valueChanges().subscribe( val => {
      this.tweetsNoconfirmado = [];
      val.forEach(e => this.tweetsNoconfirmado.push(<string>e));
    });
    this.af.list('/tweets/aborto/seabstiene').valueChanges().subscribe( val => {
      this.tweetsSeAbstiene = [];
      val.forEach(e => this.tweetsSeAbstiene.push(<string>e));
    });
  }

}
