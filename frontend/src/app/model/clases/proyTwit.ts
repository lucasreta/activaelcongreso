export class ProyTwit {
    public id: number;
    public proyecto: string;
    public twit: string;
    public posicion: string;

    public constructor(posicion?: string) {
        this.posicion = posicion;
    }

    public asignProps(pt2: ProyTwit) {
        ProyTwit.asignProps(this, pt2);
    }

    public static asignProps(pt1: ProyTwit, pt2: ProyTwit) {
        pt1.id = pt2.id;
        pt1.proyecto = pt2.proyecto;
        pt1.twit = pt2.twit;
        pt1.posicion = pt2.posicion;
    }

    public static getNew() {
        return new ProyTwit();
    }

}