import { PosLeg } from './posLeg';

export class Diputadx {

    public id: Number;
    public nombre: string;
    public apellido: string;
    public bloque: string;
    public distrito: string;
    public espacio: string;
    public imgExt: string;
    public estadoCivil: string;
    public twitter: string;
    public interno: string;
    public fechaNac: Date = new Date();
    public img: string;
    public cargo: string = 'Diputadx';
    public facebook: string;
    public numeroCompleto: number = 0;
    //Otros objetos
    public posiciones = new Array<PosLeg>();

    public asignProps(f: Diputadx) {
      Diputadx.asignProps(this, f);
    }

    public static asignProps(d1: Diputadx, d2: Diputadx) {
      d1.id = d2.id;
      d1.nombre = d2.nombre;
      d1.apellido = d2.apellido;
      d1.bloque = d2.bloque;
      d1.distrito = d2.distrito;
      d1.espacio = d2.espacio;
      d1.imgExt = d2.imgExt;
      d1.estadoCivil = d2.estadoCivil;
      d1.twitter = d2.twitter;
      d1.interno = d2.interno;
      d1.fechaNac = d2.fechaNac as Date;
      d1.img = d2.img;
      d1.cargo = d2.cargo;
      d1.facebook = d2.facebook;
      d1.numeroCompleto = d2.numeroCompleto;
      d1.posiciones = d2.posiciones as Array<PosLeg>;
    }

}