import { ProyTwit } from './proyTwit';
import { ProyVid } from './proyVid';

export class Proyecto {

    public id: string;
    public nombre: string;
    public fotoExt: string;
    public descripcion: string;
    public objetivo: string = 'sen';
    public verCont: boolean;
    public verVid: boolean;
    public verArtic: boolean;
    public img: string;
    public articulo: string;
    public contador: number = 0;
    //Otros objetos
    public twits = new Array<ProyTwit>();
    public videos = new Array<ProyVid>();

    public generateId(): void {
        let newId = this.nombre.replace(/ /gi, "-");
        newId = newId.replace(/á/gi,"a");
        newId = newId.replace(/é/gi,"e");
        newId = newId.replace(/í/gi,"i");
        newId = newId.replace(/ó/gi,"o");
        newId = newId.replace(/ú/gi,"u");
        newId = newId.replace(/ñ/gi,"n");
        this.id = newId;
    }

    public asignProps(p: Proyecto) {
        Proyecto.asignProps(this, p);
    }

    public static asignProps(p1: Proyecto, p2: Proyecto) {
        p1.id = p2.id;
        p1.nombre = p2.nombre;
        p1.fotoExt = p2.fotoExt;
        p1.descripcion = p2.descripcion;
        p1.objetivo = p2.objetivo;
        p1.verCont = Proyecto.getBooleanValue(p2.verCont);
        p1.verVid = Proyecto.getBooleanValue(p2.verVid);
        p1.verArtic = Proyecto.getBooleanValue(p2.verArtic);
        p1.img = p2.img;
        p1.articulo = p2.articulo;
        p1.contador = p2.contador;
        p1.twits = p2.twits as Array<ProyTwit>;
        p1.videos = p2.videos as Array<ProyVid>;
    }

    public static getBooleanValue(val: any): boolean {
        if(val == "1") {
            return true;
        } else if(val == "") {
            return false;
        } else {
            return val;
        }
    }

}