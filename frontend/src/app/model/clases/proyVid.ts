export class ProyVid {
    public id: number;
    public proyecto: string;
    public video: string;

    public asignProps(pv2: ProyVid) {
        ProyVid.asignProps(this, pv2);
    }

    public static asignProps(pv1: ProyVid, pv2: ProyVid) {
        pv1.id = pv2.id;
        pv1.proyecto = pv2.proyecto;
        pv1.video = pv2.video;
    }

    public static getNew() {
        return new ProyVid();
    }

}