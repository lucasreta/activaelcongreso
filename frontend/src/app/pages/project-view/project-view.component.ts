import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Proyecto } from '../../model/clases/proyectos';
import { ProyectoService } from '../../model/servicios/proyecto.service';

@Component({
    selector: 'project-view',
    templateUrl: './project-view.component.html',
    styleUrls: ['./project-view.component.scss']
})
export class ProjectViewComponent {

    public currentProy: Proyecto = new Proyecto();

    constructor(private route: ActivatedRoute,
        private proyectoService: ProyectoService) {
        this.route.paramMap.subscribe(params => {
            this.proyectoService
                .traerPorId(params['params']['id'])
                .then(p => {
                    this.currentProy = p;
                });
        });
        
    }



}