import { Component, Input, AfterViewInit, ElementRef, OnInit } from "@angular/core";
import { Diputadx } from '../../../../model/clases/diputadxs';
import { DiputadxService } from '../../../../model/servicios/diputadx.service';
import { Proyecto } from '../../../../model/clases/proyectos';
import * as $ from 'jquery';

@Component({
    selector: 'leg-slider',
    templateUrl: './leg-slider.component.html',
    styleUrls: ['./leg-slider.component.scss']
})
export class LegSliderComponent implements OnInit {

    @Input() proyecto: Proyecto;

    public listaLegis: Diputadx[];

    public constructor(public diputadxsService: DiputadxService) {   
    }

    public ngOnInit(): void {
        this.diputadxsService
            .traerPorCargo(this.proyecto.objetivo)
            .then(rta => this.listaLegis = rta.slice(0, 10));
    }

}