import { Component, Input, OnInit, OnChanges } from "@angular/core";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
    selector: 'article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnChanges {

    @Input() articulo: string;

    public artSanitarized: SafeHtml;
    public vistaPrevia: string;

    public constructor(private sanitizer: DomSanitizer) {
    }

    public ngOnChanges(): void {
        this.artSanitarized = this.sanitizer.bypassSecurityTrustHtml(this.articulo);
    }

}