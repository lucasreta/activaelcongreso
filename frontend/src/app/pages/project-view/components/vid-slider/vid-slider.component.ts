import { Component, Input, OnInit } from "@angular/core";
import { Proyecto } from '../../../../model/clases/proyectos';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
    selector: 'vid-slider',
    templateUrl: './vid-slider.component.html',
    styleUrls: ['./vid-slider.component.scss']
})
export class VidSliderComponent {

    @Input() proyecto: Proyecto;

    constructor(private sanitizer: DomSanitizer) {
    }

    public sanitarize(url: string): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

}