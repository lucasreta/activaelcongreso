import { Component, Input } from "@angular/core";

@Component({
    selector: 'contador',
    templateUrl: './contador.component.html',
    styleUrls: ['./contador.component.scss']
})
export class ContadorComponent {
    
    @Input() cantidad: number;

}