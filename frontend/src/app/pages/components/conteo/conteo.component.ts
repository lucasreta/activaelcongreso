import { Component, Input } from "@angular/core";
import { Proyecto } from '../../../model/clases/proyectos';
import { DiputadxService } from '../../../model/servicios/diputadx.service';
import { Diputadx } from '../../../model/clases/diputadxs';

@Component({
    selector: 'conteo',
    templateUrl: './conteo.component.html',
    styleUrls: ['./conteo.component.scss']
})
export class ConteoComponent {

    @Input() set proyecto(p: Proyecto) { this.onProyChanges(p)};

    public _proyecto: Proyecto;
    public aFavor: number = 0;
    public enContra: number = 0;
    public seAbstiene: number = 0;
    public noConfirmado: number = 0;

    public constructor(public polService: DiputadxService) {
    }

    public onProyChanges(p: Proyecto): void {
        if(p.objetivo) {
            this._proyecto = p;
            this.polService
                .traerPorCargo(this._proyecto.objetivo)
                .then(listaPol => this.actualizarConteo(listaPol));
        }
    }

    public actualizarConteo(listaPol: Array<Diputadx>) {
        this.aFavor = 0;
        this.enContra = 0;
        this.seAbstiene = 0;
        this.noConfirmado = 0;
        listaPol.forEach(p => {
            let pos = (p.posiciones.filter(p => p.proyecto == this._proyecto.id)[0]);
            if(pos){
                switch(pos.posicion) {
                    case 'AFavor':
                        this.aFavor++;
                        break;
                    case 'EnContra':
                        this.enContra++;
                        break;
                    case 'SeAbstiene':
                        this.seAbstiene++;
                        break;
                    case 'NoConfirmado':
                        this.noConfirmado++;
                        break;
                }
            };
            
        });
    }

    public getPorc(num: number) {
        let total = this.aFavor + this.enContra + this.noConfirmado + this.seAbstiene;
        return (num * 100) / total;
    }

    public getEnd(): string {
        let end;
        if(this._proyecto.objetivo) {
            if(this._proyecto.objetivo == 'sen') {
                end = 'en el senado';
            } else if(this._proyecto.objetivo == 'dip') {
                end = 'en diputadxs';
            } else if(this._proyecto.objetivo = 'leg') {
                end = 'en la legislatura';
            }
        }
        return end;
     }

}