import { Component, Input, OnInit } from "@angular/core";
import { Proyecto } from '../../../../model/clases/proyectos';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../../../environments/environment';

@Component({
    selector: 'project-card',
    templateUrl: 'project-card.component.html',
    styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent implements OnInit {
    
    @Input() project: Proyecto;
    public bkg: string;
    
    public constructor(public sanitizer: DomSanitizer) {
    }

    public sanitize(style): any {
        return this.sanitizer.bypassSecurityTrustStyle(style);
    }

    public ngOnInit(): void {
        let bkgAux = 'linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url(' + environment.imgBase + this.project.img + ') no-repeat';
        this.bkg = this.sanitize(bkgAux);
    }

}