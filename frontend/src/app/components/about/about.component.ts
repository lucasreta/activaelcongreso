import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

export class Video { 
  public videoSafeUrl: SafeResourceUrl;
  constructor(
    videoLink: string,
    sanitizer: DomSanitizer,
    public title: string,
    public description?: string) {
      this.videoSafeUrl = sanitizer.bypassSecurityTrustResourceUrl(videoLink);
  }
}

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {

}