import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

export class Video { 
  public videoSafeUrl: SafeResourceUrl;
  constructor(
    videoLink: string,
    sanitizer: DomSanitizer,
    public title: string,
    public description?: string) {
      this.videoSafeUrl = sanitizer.bypassSecurityTrustResourceUrl(videoLink);
  }
}

@Component({
  selector: 'about-reform',
  templateUrl: './about-reform.component.html',
  styleUrls: ['./about-reform.component.scss']
})
export class AboutReformComponent {
  
  public videos: Array<Video>;
  public currentVideoTitle: string;
  public currentIframeUrl: SafeResourceUrl;

  public constructor(private sanitizer: DomSanitizer) {
    this.videos = new Array<Video>();
    this.videos.push(new Video(
      'https://www.youtube.com/embed/e1m9FaYsuHc',
      sanitizer,
      'Ahora que sí nos ven'
    ));
    this.videos.push(new Video(
      'https://www.youtube.com/embed/ZygqHHvnIkQ',
      sanitizer,
      'Luciana Peker - Exposición en Congreso'
    ));
    //Vicki
    this.videos.push(new Video(
      'https://www.youtube.com/embed/u7qsUC5hGOs',
      sanitizer,
      'Victoria Freire: Debate en los medios'
    ));
    //TV Pública
    this.videos.push(new Video(
      'https://www.youtube.com/embed/ebdqYIxlXWU',
      sanitizer,
      'Campaña Nacional por el Derecho al Aborto Legal, Seguro y Gratuito'
    ));
    this.videos.push(new Video(
      'https://www.youtube.com/embed/MLJear0RhmI',
      sanitizer,
      'Alberto Kornblihtt - Exposición'
    ));
    this.videos.push(new Video(
      'https://www.youtube.com/embed/1AcXDGegMwM',
      sanitizer,
      'Gabriela Cerruti - Exposición en Diputados'
    ));
    this.videos.push(new Video(
      'https://www.youtube.com/embed/Mse-nz5mBxU',
      sanitizer,
      'Silvia Lospennato - Exposición en Diputados'
    ));
    this.videos.push(new Video(
      'https://www.youtube.com/embed/AlXlE-OpwAI',
      sanitizer,
      'Victoria Donda - Exposición en Diputados'
    ));
    this.videos.push(new Video(
      'https://www.youtube.com/embed/psG6ly1_rX4',
      sanitizer,
      'Mayra Mendoza - Exposición en Diputados'
    ));
  }

  public setCurrentIframeUrl(video: Video): void {
    this.currentVideoTitle = video.title;
    this.currentIframeUrl = video.videoSafeUrl;
  }

  public resetModalVariables(): void {
    this.currentVideoTitle = null;
    this.currentIframeUrl = null;
  }

}