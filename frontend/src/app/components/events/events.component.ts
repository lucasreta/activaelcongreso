import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  
  public isXs: boolean;

  public ngOnInit(): void {
    this.setIsXs();
    $(window).resize(this.setIsXs.bind(this));
  }

  public setIsXs(): void {
    this.isXs = $(window).width() < 576;
  }

}