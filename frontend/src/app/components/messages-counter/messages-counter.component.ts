import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import * as _ from "lodash";


@Component({
  selector: 'messages-counter',
  templateUrl: './messages-counter.component.html',
  styleUrls: ['./messages-counter.component.scss']
})
export class MessagesCounterComponent implements OnInit {

  public isXs: boolean;
  public _messages: number = 0;
  public _messagesString: string;

  @Input() set messages(messages: number){
    this._messages = messages;
    this._messagesString = new Intl.NumberFormat('es-ar').format(messages);
  };

  public ngOnInit(): void {
    this.setIsXs();
    $(window).resize(this.setIsXs.bind(this));
  }

  public setIsXs(): void {
    this.isXs = $(window).width() < 576;
  }

}
