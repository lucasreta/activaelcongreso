import { Component, Input, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
//import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import * as _ from "lodash";
import { environment } from '../../../environments/environment';
import { Legislator } from '../../interfaces/legislator';

@Component({
  selector: 'app-legisladores',
  templateUrl: './legisladores.component.html',
  styleUrls: ['./legisladores.component.scss']
})
export class LegisladoresComponent {

  subtitulo = "#ABORTOLEGALYA #8A"

  @Input() set diputadxs(lista: Legislator[]){
    //_.remove(lista, ['posicion', "En Contra"]);
    //_.remove(lista, ['posicion', "A Favor"]);
    //_.remove(lista, ['posicion', "Se Abstiene"]);
    //_.remove(lista, ['posicion', "No confirmado"]);
    //lista = _.remove(lista, 'twitter');
    _.remove(lista, ['disabled', true]);

    let primerxs = _.remove(lista, dipu => dipu.orden <= 1);
    primerxs = _.map(primerxs, (dipu, index) => {
      if (dipu.orden != 0) {
        dipu.orden = this.ordenesRandom[index];
      }
      return dipu;
    });
    primerxs = _.sortBy(primerxs, 'orden');

    lista = _.map(lista, (dipu, index) => {
      dipu.orden = this.ordenesRandom[index];
      return dipu;
    });
    lista = _.sortBy(lista, 'orden');

    lista = _.slice(lista, 0, 25);

    this._items = _.concat(primerxs, lista);
  };

  public _items: Legislator[];
  public currentDiputadx: Legislator;
  private ordenesRandom: number[];

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document: any) {
    this.ordenesRandom = _.shuffle(_.range(0, 257));
  }


  public setCurrentDiputadx(dipu: any): void {
    this.currentDiputadx = dipu;
  }

  public twittearAborto(dipu: Legislator) {
    const url = environment.contarTweetUrl;
    this.http.post(url, {id: dipu.key, tipoLegis:dipu.dbpath, campaign:'aborto'}, {responseType: 'text'}).subscribe(res => console.log(res));
    let tweet = dipu.abortionTweet();
    window.open(`https://twitter.com/intent/tweet?text=${tweet}`, "_blank");
  }

  public contarLlamado(dipu: Legislator) {
    const url = environment.contarLlamadoUrl;
    this.http.post(url, {id: dipu.key, tipoLegis:dipu.dbpath, campaign:'aborto'}, {responseType: 'text'}).subscribe(res => console.log(res));
  }

  public facebookAborto(dipu: Legislator) {
    const url = environment.contarFacebookUrl;
    this.http.post(url, {id: dipu.key, tipoLegis:dipu.dbpath, campaign:'aborto'}, {responseType: 'text'}).subscribe(res => console.log(res));
    //window.open(`https://m.me/${dipu.facebook}`, "_blank");
    window.open(`https://facebook.com/${dipu.facebook}`, "_blank");
  }

}
