import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

declare var $: any;

@Component({
  selector: 'propuestas-page',
  templateUrl: './propuestas.component.html',
  styleUrls: ['./propuestas.component.scss']
})
export class PropuestasComponent implements OnInit {

  public routeSub: Subscription;

  constructor(private router: Router) { 
  }

  public ngOnInit(): void {
    let param = document.location.toString().split('#')[1];
    if(this.router.url === '/propuestas' && param) {
      this.openModalById(param);
    }
  }

  private openModalById(id: string) {
    
  }

}