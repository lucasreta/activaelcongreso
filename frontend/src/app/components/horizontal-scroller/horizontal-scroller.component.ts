import { Component, OnInit, ElementRef, AfterViewInit, Input } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-horizontal-scroller',
  templateUrl: './horizontal-scroller.component.html',
  styleUrls: ['./horizontal-scroller.component.scss']
})
export class HorizontalScrollerComponent implements OnInit, AfterViewInit {

  @Input() bgColor: string;
  @Input() titleFirstPart: string;
  @Input() titleSecondPart: string;
  @Input() subtitle: string;
  @Input() descriptionText: string[];

  public isBlackArrow: boolean = false;

  constructor(private el: ElementRef) { }

  $scrollableWrapper: JQuery | any;
  $leftPaddle: JQuery | any;
  $rightPaddle: JQuery | any;

  public ngOnInit(): void {
    if(this.bgColor == "#FFF") {
      this.isBlackArrow = true;
    }
  }

  public scrollHorizontal(direction: string): void {
    let leftPos = this.$scrollableWrapper.scrollLeft();
    let newPos;
    if(direction == "l") {
      newPos = leftPos - 500;
    } else if (direction == "r") {
      newPos = leftPos + 500;
    }
    this.$scrollableWrapper.animate({ scrollLeft: newPos },{ duration: 130 });
  }

  ngAfterViewInit() {
    let sliderEl = $(this.el.nativeElement).children(".scroller-horizontal-container").children(".slider");
  	this.$scrollableWrapper = sliderEl.children(".scrolling-wrapper-flexbox");
  	this.$leftPaddle = sliderEl.children(".handlePrev");
  	this.$rightPaddle = sliderEl.children(".handleNext");
  }

}
