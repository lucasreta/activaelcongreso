import json
import csv
import uuid
import unidecode

if __name__ == "__main__":
	senadorxs = {}
	with open('senadorxs.csv') as senafile:
		senareader = csv.DictReader(senafile)
		for row in senareader:
			senador = {
				'name': row['NOMBRE'].title(),
				'lastname': row['APELLIDO'].title(),
				'genero': row['sexo'],
				'birth': row['nacimiento'],
				'ec': row['estado civil'],
				'espacio': row['espacio'],
				'partido': row['partido'].title(),
				'bloque': row['bloque'].title(),
				'mail': row['mail'],
				'telefono': row['telefono'],
				'posicion': row['posicion'],
				'img': row['img'],
				'distrito': row['distrito']
			}
			if senador['bloque'] == 'Frente Pro':
				senador['bloque'] = 'PRO'
			if row['facebook']:
				senador['facebook'] = row['facebook'].rstrip('/')
			if row['instagram']:
				senador['instagram'] = row['instagram'].rstrip('/')
			if row['twitter']:
				senador['twitter'] = row['twitter'].lstrip('@')
			if row['youtube']:
				senador['youtube'] = row['youtube']

			key = unidecode.unidecode(''.join(senador['lastname'].lower().split()) + '_' + ''.join(senador['name'].lower().split()) + '_' + str(uuid.uuid4()).split('-')[-1])
			for c in ['$', '#', '[', ']', '/', '.']:
				if c in key:
					print(f'ERROR {senador}')
			senadorxs[key] = senador
	
	diputadxs = {}
	counters = {
		'aborto': {
			'senadorxs': {},
			'diputadxs': {},
			'total': {
				'tweets': 0,
				'llamados': 0
			}
		},
		'total': {
			'tweets': 0,
			'llamados': 0
		}
	}
	total_tweets = 0
	total_llamados = 0
	with open('./backups/noalareformalaboral-a655b-backup20180619-1144.json') as dipufile:
		data = json.load(dipufile)
	
	diputadxs_planilla = {}
	with open('./diputadxs.csv') as dipucsvfile:
		dipureader = csv.DictReader(dipucsvfile)
		for row in dipureader:
			key = row['Diputado']
			diputadx = {
				'genero': row['sexo'],
				'birth': row['nacimiento2'],
				'ec': row['estadocivil'],
				'espacio': row['Fuerza'],
				'distrito': row['distrito']
			}
			diputadxs_planilla[key] = diputadx

	for key, val in data['diputadxs'].items():
		key_planilla = val['lastname'] + ', ' + val['name']
		dipu_planilla = diputadxs_planilla[key_planilla]
		diputadx = {
			'name': val['name'],
			'lastname': val['lastname'],
			'genero': dipu_planilla['genero'],
			'birth': dipu_planilla['birth'],
			'ec': dipu_planilla['ec'],
			'espacio': dipu_planilla['espacio'],
			'bloque': val['bloque'].title(),
			'posicion': val['posicion'],
			'img': val['img'],
			'distrito': dipu_planilla['distrito']
		}
		if diputadx['bloque'] == 'Pro':
			diputadx['bloque'] = 'PRO'
		if 'interno' in val:
			diputadx['telefono'] = val['interno']
		newkey = unidecode.unidecode(''.join(diputadx['lastname'].lower().split()) + '_' + ''.join(diputadx['name'].lower().split()) + '_' + str(uuid.uuid4()).split('-')[-1])

		counters['aborto']['diputadxs'][newkey] = {
			'tweets': val['tweets'] if 'tweets' in val else 0,
			'llamados': val['llamados'] if 'llamados' in val else 0
		}
		total_llamados += val['llamados'] if 'llamados' in val else 0
		total_tweets += val['tweets'] if 'tweets' in val else 0
		for c in ['$', '#', '[', ']', '/', '.']:
				if c in key:
					print(f'ERROR {diputadx}')
		diputadxs[newkey] = diputadx

	counters['total']['tweets'] = total_tweets
	counters['total']['llamados'] = total_llamados
	counters['aborto']['total']['tweets'] = total_tweets
	counters['aborto']['total']['llamados'] = total_llamados
	data['diputadxs'] = diputadxs
	data['senadorxs'] = senadorxs
	data['counters'] = counters

	with open('./export.json', 'w') as outfile:
		json.dump(data, outfile)
