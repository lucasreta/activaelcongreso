# Activá el Congreso

https://activaelcongreso.org/

## Entorno dev:

Lo más fácil es tener instalado [yarn](https://yarnpkg.com/), y entonces simplemente ir con la consola al directorio `frontend` y ejecutar:

    yarn
    yarn start


## Deployment

  cd frontend
  yarn build
  cd ..
  firebase deploy
